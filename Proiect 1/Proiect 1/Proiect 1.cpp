// Proiect 1.cpp : Pentru un automat finit si un cuvant dat, sa se decida daca acel cuvant apartine sau nu limbajului recunoscut de automat.
//

#include "stdafx.h"

struct faza
{
	int init;
	int final;
	char c;
};

int verificare(vector <faza> faze, string s, int stareinit, string alfabet, vector <int> starifin)
{

	int ok = 0;
	if (s == "*")
	{
		for (int i = 0; i < starifin.size(); i++)
		{

			if (stareinit == starifin[i])
			{
				cout << "Cuvantul apartine" << endl;
				return 0;
			}

		}
		cout << "Cuvantul nu apartine" << endl;
		return 0;
	}

	for (int i=0; i<s.size(); i++)
	{
	
		for (int j =0; (j) < alfabet.size(); (j)++)
			if (s[i] == alfabet[j])
				ok = 1;
		if (ok == 0)
		{
			cout << "Cuvantul nu apartine" << endl;
			return 0;
		}
		int ok1 = 0;
		for (int a = 0; (a) < faze.size(); a++)
		{

			if (s[i] == faze[a].c && stareinit == faze[a].init)
			{
				stareinit = faze[a].final;
				ok1 = 1;
				break;	
			}
		}

		if(ok1==0)
		{
			cout << "Cuvantul nu apartine" << endl;
			return 0;
		}

	}
	cout << stareinit << endl;

	for (int i = 0; i < starifin.size(); i++)
	{
		cout<<starifin[i]<<endl;

		if (stareinit == starifin[i])
		{
			cout << "Cuvantul apartine" << endl;
			return 0;
		}
	}
	cout << "cuvantul nu apartine";
	return 0;
}

int main()
{	
	//Citire cuvant
	string s;
	ifstream f("automat.in");
	f >> s;

	//Citire stari
	vector <int> stari;
	int contor,x;
	f >> contor;
	for (int i = 0; i < contor; i++)
	{
		f >> x;
		stari.push_back(x);
	}

	//Citire alfabet
	string alfabet;
	f >> alfabet;
	//Citire stare initiala

	int stareinit;
	f >> stareinit;

	//Citire stari finale

	f >> contor;
	vector <int>starifin(contor);
	for (int i = 0; i < contor; i++)
		f >> starifin[i];
	
	//Citire functii tranzitie
	
	f >> contor;
	vector <faza> faze(contor);
	for (int i = 0; i < contor; i++)
	{
		f >> faze[i].init;
		f >> faze[i].final;
		f >> faze[i].c;
	}

	verificare(faze, s, stareinit, alfabet, starifin);

    return 0;
}

